﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MTaskInterface
{
    // расчет зоны прямой видимости
    interface ILSR
    {
        
        // координаты АСП комплекса
        List<ModelsTablesDBLib.TableASP> LJammer { get; set; }

              
        // шаг по расстоянию
        byte StepLength { get; set; }

        // шаг по углу
        byte StepAzimuth { get; set; }

        // выполнить 
        void Accept();

        // очистить
        void Clear();

        // результат расчета ЗПВ для отрисовки на карте
        List<ModelsTablesDBLib.Coord> LPointLSR { get; }

        // дальность
        short Range { get;}

        // средняя высота
        short AverageHeight { get; }

    
    }
}
