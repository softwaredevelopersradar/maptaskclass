﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MTaskInterface.LSR
{
    // объект подавления
    interface IObjectLSR
    {
        // высота антенны объекта подавления
        short Antenna { get; set; }

        // суммарная высота объекта подавления
        short TotalHeight { get; }
    }
}
