﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MTaskInterface.LSR
{
    // средство подавления
    interface IMeansLSR
    {
        // координаты центра зоны
        ModelsTablesDBLib.Coord Coord { get; set; }

        // высота антенны средства подавления
        short Antenna { get; set; }

        // суммарная высота средства подавления
        short TotalHeight { get; }
    }
}
