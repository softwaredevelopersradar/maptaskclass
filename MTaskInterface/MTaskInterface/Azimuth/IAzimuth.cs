﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MTaskInterface
{
    // расчет азимута на заданную точку 
    interface IAzimuth
    {
        // координаты точки
        ModelsTablesDBLib.Coord CoordPoint { get; set;}

        // координаты АСП комплекса
        List<ModelsTablesDBLib.TableASP> LJammer { get; set; }

        // признак отображения численного значения на карте
        bool ShowValue { get; set;}        
                
        // выполнить 
        void Accept();

        // очистить
        void Clear();

        // результат расчета азимута
        List<float> LAzimuth { get; }

    }
}
