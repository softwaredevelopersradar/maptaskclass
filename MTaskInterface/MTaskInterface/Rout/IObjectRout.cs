﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MTaskInterface.Rout
{
    interface IObjectRout
    {
        //  признак отрисовки на карте
        bool ShowMap { get; set; }
       
        // протяженность
        double Distance { get;}
       
    }
}
