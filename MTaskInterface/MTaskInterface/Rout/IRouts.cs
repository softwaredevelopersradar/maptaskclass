﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MTaskInterface.Rout
{
    interface IRouts
    {
        void Add();

        void Delete();

        void Clear();
    }
}
