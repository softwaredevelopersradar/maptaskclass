﻿using ModelsTablesDBLib;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace MTaskInterface.Rout
{
    public class ObjectRout : TableRoute, IObjectRout, INotifyPropertyChanged
    {
       
        private double _distance = -1;


        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }


        public double Distance
        {
            get { return _distance; }
            set
            {
                if (_distance == value)
                    return;
               
                _distance = value;
                OnPropertyChanged(nameof(Distance));
            }
            
        }


        public bool ShowMap { get; set; }




    }
}
