﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MTaskInterface
{
    // объект подавления
    interface IObjectPAZ
    {
        // координаты объекта подавления
        ModelsTablesDBLib.Coord Coord { get; set; }

        // высота антенны приемника
        short AntennaRX { get; set; }

        // высота антенны передатчика
        short AntennaTX { get; set; }

        // мощность объекта подавления
        short Power { get; set; }

        // коэффициент усиления объекта подавления
        byte Gain { get; set; }

        // коэффициент подавления объекта подавления
        byte Coeff { get; set; }
    }
}
