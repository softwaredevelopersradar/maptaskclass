﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MTaskInterface
{
    // расчет зоны энергодоступности по зонам
    interface IPAZ
    {        
        // координаты АСП комплекса
        List<ModelsTablesDBLib.TableASP> LJammer { get; set; }

        // выполнить 
        void Accept();

        // очистить
        void Clear();

        // средняя высота
        short AverageHeight { get; }

        // радиус зоны подавления
        int  RadiusZS{ get; }

        // радиус зоны неподавления
        int RadiusZNS { get; }
    }
}
