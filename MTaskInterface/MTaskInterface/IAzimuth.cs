﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MTaskInterface
{
    interface IAzimuth
    {
        // координаты точки
        ModelsTablesDBLib.Coord PointCoord { get; set;}

        // координаты АСП комплекса
        List<ModelsTablesDBLib.TableASP> LJammer { get; set; }

        // признак отображения численного значения на карте
        bool ShowValue { get; set;}        
        
        // результат расчета азимута
        List<float> LAzimuth { get;}

        // выполнить 
        void Accept();

        // очистить
        void Clear();
        bool cg { get; set; }
    }
}
