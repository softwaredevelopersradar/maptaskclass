﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MTaskInterface
{
    // объект подавления
    interface IObjectPACC
    {
        // координаты УС 1
        List<ModelsTablesDBLib.Coord> Coord { get; set; }
        
        // несущая частота
        double Frequency { get; set; }

        // мощность передатчика
        short Power { get; set; }

        // коэффициент усиления 
        short Gain { get; set; }

        // коэффициент подавления
        float Coeff { get; set; }

        // дальность связи
        int CommRange { get; set; }

        // поляризация
        byte Polarization { get; set; }

        // ширина спектра 
        float Deviation { get; set; }

        // высота антенны приемника
        short AntennaRX { get; set; }

        // высота антенны передатчика
        short AntennaTX { get; set; }


    }
}
