﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MTaskInterface
{
    // средство подавления
    interface IMainsPACC
    {
        // координаты центра зоны
        ModelsTablesDBLib.Coord Coord { get; set; }

        // высота антенны средства подавления
        short Antenna { get; set; }

        // суммарная высота средства подавления
        short TotalHeight { get; }

        // мощность средства подавления
        short Power { get; set; }

        // коэффициент усиления средства подавления
        byte Gain { get; set; }

        // ширина спектра помехи
        float Deviation { get; set; }

        // подстилающая поверхность
        byte Surface { get; set; }
    }
}
