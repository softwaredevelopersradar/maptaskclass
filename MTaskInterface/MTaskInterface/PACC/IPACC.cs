﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MTaskInterface
{
    // расчет зоны энергодоступности по узлам связи
    interface IPACC
    {
       
        // координаты АСП комплекса
        List<ModelsTablesDBLib.TableASP> LJammer { get; set; }

        // выполнить 
        void Accept();

        // очистить
        void Clear();

        // дальность РП
        int SupRange { get; }

        // дальность прямой видимости
        int DSR { get; }

        // средняя высота
        short AverageHeight { get; }

        List <float> CoeffHS { get; }
    }
}
