﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MTaskInterface
{
    // расчет зоны равных ошибок
    interface IZEM
    {        
        // координаты АСП комплекса
        List<ModelsTablesDBLib.TableASP> LJammer { get; set; }
        
        // выполнить 
        void Accept();

        // очистить
        void Clear();

        // база пеленгования
        int RangeBase { get;}

        // результат расчета ЗПВ для отрисовки на карте пеленгатор 1
        List<ModelsTablesDBLib.Coord> LPoint1ZEM { get; }

        // результат расчета ЗПВ для отрисовки на карте пеленгатор 2
        List<ModelsTablesDBLib.Coord> LPoint2ZEM { get; }

    }
}
