﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MTaskInterface.ZEM
{
    // средство подавления
    interface IMeansZEM
    {
        // координаты пеленгатора 1
        ModelsTablesDBLib.Coord CoordMeans1 { get; set; }

        // координаты пеленгатора 2
        ModelsTablesDBLib.Coord CoordMeans2 { get; set; }

        // шаг
        short StepLength { get; set; }

        // максимальная дальность
        int Range { get; set; }

        // СКО
        short RMS { get; set; }

    }
}
